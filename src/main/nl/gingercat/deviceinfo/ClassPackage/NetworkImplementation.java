package main.nl.gingercat.deviceinfo.ClassPackage;

import android.content.Context;
import android.telephony.TelephonyManager;
import main.nl.gingercat.deviceinfo.Abstracts.Network;

/**
 * Created by jacek on 11/18/15.
 */
public class NetworkImplementation extends Network {

    Context context;
    TelephonyManager telephonyManager;


    public NetworkImplementation(Context context){
        this.context = context;
        telephonyManager = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
    }

    @Override
    public String getNetworkOperatorName() {
        String operatorName = telephonyManager.getNetworkOperatorName();
        return operatorName;
    }

    @Override
    public String getSimOperatorName() {
        return telephonyManager.getSimOperatorName();
    }

    @Override
    public String getNetworkOperatorID() {
        return null;
    }

    @Override
    public String getSubscriberId() {
        return telephonyManager.getSubscriberId();
    }

    @Override
    public int getPhoneType() {
        return telephonyManager.getPhoneType();
    }

    @Override
    public String getDeviceId() {
        return telephonyManager.getDeviceId();
    }

    @Override
    public int getDataState() {
        return telephonyManager.getDataState();
    }
}
