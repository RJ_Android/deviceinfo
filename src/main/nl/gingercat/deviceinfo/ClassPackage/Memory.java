package main.nl.gingercat.deviceinfo.ClassPackage;

import android.app.ActivityManager;
import main.nl.gingercat.deviceinfo.Abstracts.Storage;

/**
 * Created by jacek on 11/18/15.
 */
public class Memory extends Storage {

    ActivityManager.MemoryInfo memoryInfo;

    public Memory() {
        memoryInfo = new ActivityManager.MemoryInfo();
    }

    @Override
    public String getMemoryInfo() {
        return null;
    }

    @Override
    public Long getAvilableMemory() {
        return memoryInfo.availMem;
    }

    @Override
    public Long getUsedMemory() {
        return null;
    }

    @Override
    public Long getFreeMemory() {
        return null;
    }

    @Override
    public Long getTotalMemory() {
        return memoryInfo.totalMem;
    }
}
