package main.nl.gingercat.deviceinfo.ClassPackage;

import android.os.Build;

/**
 * Created by jacek on 11/18/15.
 */
public class Device implements main.nl.gingercat.deviceinfo.Interfaces.Device {

    @Override
    public String codeName() {
        return Build.DEVICE;
    }

    @Override
    public String brandName() {
        return Build.MANUFACTURER;
    }

    @Override
    public String modelName() {
        return Build.MODEL;
    }

    @Override
    public String serialNumber() {
        return Build.SERIAL;
    }

    @Override
    public String deviceBuildId() {
        return Build.DISPLAY;
    }
}
