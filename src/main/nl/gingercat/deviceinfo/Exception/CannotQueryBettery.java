package main.nl.gingercat.deviceinfo.Exception;

/**
 * Created by Jacky on 2015-11-15.
 */
public class CannotQueryBettery extends Exception {
    public CannotQueryBettery() {
        super();
    }

    public CannotQueryBettery(String detailMessage) {
        super(detailMessage);
    }

    public CannotQueryBettery(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CannotQueryBettery(Throwable throwable) {
        super(throwable);
    }
}
