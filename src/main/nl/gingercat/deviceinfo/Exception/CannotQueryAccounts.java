package main.nl.gingercat.deviceinfo.Exception;

/**
 * Created by Jacky on 2015-11-15.
 */
public class CannotQueryAccounts extends Exception {
    public CannotQueryAccounts() {
        super();
    }

    public CannotQueryAccounts(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CannotQueryAccounts(String detailMessage) {
        super(detailMessage);
    }

    public CannotQueryAccounts(Throwable throwable) {
        super(throwable);
    }
}
