package main.nl.gingercat.deviceinfo.Interfaces;

import main.nl.gingercat.deviceinfo.Exception.CannotQueryWifi;

/**
 * Created by jacek on 11/18/15.
 */
public interface Connectivity {

    String getMacAddress() throws CannotQueryWifi;
}
