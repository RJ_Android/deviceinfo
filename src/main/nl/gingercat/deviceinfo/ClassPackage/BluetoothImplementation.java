package main.nl.gingercat.deviceinfo.ClassPackage;

import android.bluetooth.BluetoothAdapter;
import main.nl.gingercat.deviceinfo.Abstracts.Bluetooth;

/**
 * Created by jacek on 11/18/15.
 */
public class BluetoothImplementation extends Bluetooth {
    @Override
    public String getMacAddress() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(bluetoothAdapter == null){
            return "Bluetooth not found in device";
        }
        return bluetoothAdapter.getAddress();
    }

}
