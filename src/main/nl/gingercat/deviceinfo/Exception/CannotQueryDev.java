package main.nl.gingercat.deviceinfo.Exception;

/**
 * Created by Jacky on 2015-11-15.
 */
public class CannotQueryDev extends Exception {
    public CannotQueryDev() {
        super();
    }

    public CannotQueryDev(String detailMessage) {
        super(detailMessage);
    }

    public CannotQueryDev(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CannotQueryDev(Throwable throwable) {
        super(throwable);
    }
}
