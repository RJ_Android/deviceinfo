package main.nl.gingercat.deviceinfo.ClassPackage;

import android.os.Build;

/**
 * Created by jacek on 11/18/15.
 */
public class DeviceOS implements main.nl.gingercat.deviceinfo.Interfaces.DeviceOS {

    @Override
    public String systemVersion() {
        return Build.VERSION.RELEASE;
    }

    @Override
    public int systemSDKVersion() {
        return Build.VERSION.SDK_INT;
    }
}
