package main.nl.gingercat.deviceinfo.Abstracts;

/**
 * Created by jacek on 11/18/15.
 */
public abstract class Network {

    public abstract String getNetworkOperatorName();
    public abstract String getSimOperatorName();
    public abstract String getNetworkOperatorID();
    public abstract String getSubscriberId();
    public abstract int getPhoneType();
    public abstract String getDeviceId();
    public abstract int getDataState();

}
