package main.nl.gingercat.deviceinfo.Interfaces;

/**
 * Created by jacek on 11/18/15.
 */
public interface Device {

    String codeName();

    String brandName();

    String modelName();

    String serialNumber();

    String deviceBuildId();

}
