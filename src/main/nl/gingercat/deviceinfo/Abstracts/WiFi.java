package main.nl.gingercat.deviceinfo.Abstracts;

import main.nl.gingercat.deviceinfo.Exception.CannotQueryWifi;
import main.nl.gingercat.deviceinfo.Interfaces.Connectivity;

/**
 * Created by jacek on 11/18/15.
 */
public abstract class WiFi implements Connectivity {

    public abstract String getMacAddress() throws CannotQueryWifi;

    public abstract boolean wifiExist();

    public abstract String getCurrentConnectedSSID();
}
