package main.nl.gingercat.deviceinfo.main;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import main.nl.gingercat.deviceinfo.ClassPackage.*;
import main.nl.gingercat.deviceinfo.Exception.*;
import nl.gingercat.DeviceInfo.R;

public class MainActivity extends Activity {
    final static String VERSION_NUMBER = "1.0";
    final static int SDK_INT = Build.VERSION.SDK_INT;

    boolean deviceNode, systemNode, networkNode, wifiNode, blueNode, batteryNode, accNode, memoryNode, devNode, extraNode;
    int nodeNumber = 10;
    LinearLayout layoutList[] = new LinearLayout[nodeNumber];
    RelativeLayout adView;
    Button buttonList[] = new Button[nodeNumber];
    TextView batteryInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        adView = (RelativeLayout) findViewById(R.id.ad_view);
        this.registerReceiver(this.batteryInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
        batteryInfo = (TextView) findViewById(R.id.m_node6_line1);

        View infoButton = findViewById(R.id.infoButton);
        infoButton.setOnClickListener(elementOnClickListener);

        initLayoutNodes();
        initButtons();

        fillNodes();
    }

    View.OnClickListener elementOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.button_deviceNode:
                    if (deviceNode) {
                        collapse(layoutList[0]);
                        deviceNode = !deviceNode;
                    } else {
                        expand(layoutList[0]);
                        deviceNode = !deviceNode;
                    }
                    break;
                case R.id.button_systemNode:
                    if (systemNode) {
                        collapse(layoutList[1]);
                        systemNode = !systemNode;
                    } else {
                        expand(layoutList[1]);
                        systemNode = !systemNode;
                    }
                    break;
                case R.id.button_networkNode:
                    if (networkNode) {
                        collapse(layoutList[2]);
                        networkNode = !networkNode;
                    } else {
                        expand(layoutList[2]);
                        networkNode = !networkNode;
                    }
                    break;
                case R.id.button_wifiNode:
                    if (wifiNode) {
                        collapse(layoutList[3]);
                        wifiNode = !wifiNode;
                    } else {
                        expand(layoutList[3]);
                        wifiNode = !wifiNode;
                    }
                    break;
                case R.id.button_blueNode:
                    if (blueNode) {
                        collapse(layoutList[4]);
                        blueNode = !blueNode;
                    } else {
                        expand(layoutList[4]);
                        blueNode = !blueNode;
                    }
                    break;
                case R.id.button_batteryNode:
                    if (batteryNode) {
                        collapse(layoutList[5]);
                        batteryNode = !batteryNode;
                    } else {
                        expand(layoutList[5]);
                        batteryNode = !batteryNode;
                    }
                    break;
                case R.id.button_accNode:
                    if (accNode) {
                        collapse(layoutList[6]);
                        accNode = !accNode;
                    } else {
                        expand(layoutList[6]);
                        accNode = !accNode;
                    }
                    break;
                case R.id.button_memoryNode:
                    if (memoryNode) {
                        collapse(layoutList[7]);
                        memoryNode = !memoryNode;
                    } else {
                        expand(layoutList[7]);
                        memoryNode = !memoryNode;
                    }
                    break;
                case R.id.button_devNode:
                    if (devNode) {
                        collapse(layoutList[8]);
                        devNode = !devNode;
                    } else {
                        expand(layoutList[8]);
                        devNode = !devNode;
                    }
                    break;
                case R.id.button_extraNode:
                    if (extraNode) {
                        collapse(layoutList[9]);
                        extraNode = !extraNode;
                    } else {
                        expand(layoutList[9]);
                        extraNode = !extraNode;
                    }
                    break;
                case R.id.infoButton:
                    startActivity(new Intent(getApplicationContext(), About.class));
                    break;
            }
        }
    };

    private void expand(LinearLayout layout) {
        layout.setVisibility(View.VISIBLE);
    }

    private void hideNode(int layout){
        buttonList[layout].setVisibility(View.GONE);
        layoutList[layout].setVisibility(View.GONE);
    }

    private void collapse(LinearLayout layout) {
        layout.setVisibility(View.GONE);
    }

    private void initLayoutNodes() {
        layoutList[0] = (LinearLayout) findViewById(R.id.layout_device);
        layoutList[1] = (LinearLayout) findViewById(R.id.layout_system);
        layoutList[2] = (LinearLayout) findViewById(R.id.layout_network);
        layoutList[3] = (LinearLayout) findViewById(R.id.layout_wifi);
        layoutList[4] = (LinearLayout) findViewById(R.id.layout_bluetooth);
        layoutList[5] = (LinearLayout) findViewById(R.id.layout_battery);
        layoutList[6] = (LinearLayout) findViewById(R.id.layout_accounts);
        layoutList[7] = (LinearLayout) findViewById(R.id.layout_memory);
        layoutList[8] = (LinearLayout) findViewById(R.id.layout_dev);
        layoutList[9] = (LinearLayout) findViewById(R.id.layout_extra);
        deviceNode = false;
        systemNode = false;
        networkNode = false;
        wifiNode = false;
        blueNode = false;
        batteryNode = false;
        accNode = false;
        memoryNode = false;
        devNode = false;
        extraNode = false;
        collapseAll(layoutList);
    }

    private void collapseAll(LinearLayout[] list) {
        for (LinearLayout l : list) {
            l.setVisibility(View.GONE);
        }
    }

    private void initButtons() {
        buttonList[0] = (Button) findViewById(R.id.button_deviceNode);
        buttonList[1] = (Button) findViewById(R.id.button_systemNode);
        buttonList[2] = (Button) findViewById(R.id.button_networkNode);
        buttonList[3] = (Button) findViewById(R.id.button_wifiNode);
        buttonList[4] = (Button) findViewById(R.id.button_blueNode);
        buttonList[5] = (Button) findViewById(R.id.button_batteryNode);
        buttonList[6] = (Button) findViewById(R.id.button_accNode);
        buttonList[7] = (Button) findViewById(R.id.button_memoryNode);
        buttonList[8] = (Button) findViewById(R.id.button_devNode);
        buttonList[9] = (Button) findViewById(R.id.button_extraNode);
        setClickListenersOnButtons(buttonList);
    }

    private void setClickListenersOnButtons(Button[] buttonList) {
        for (Button b : buttonList) {
            b.setOnClickListener(elementOnClickListener);
        }
    }

    private void fillNodes() {
        Log.i("#DeviceNode ", " Filling DeviceNode ");
        Context context = getApplicationContext();
        try {
            fillDeviceNode();
        } catch (CannotQueryDevice e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        Log.i("#SystemNode ", " Filling SystemNode ");
        try {
            fillSystemNode();
        } catch (CannotQuerySystem e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        Log.i("#WiFi ", " Filling WiFi ");
        try {
            fillWiFiNode();
        } catch (CannotQueryWifi e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        Log.i("#Bluetooth ", " Filling Bluetooth ");
        try {
            fillBluetoothNode();
        } catch (CannotCheckBluetooth e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        try {
            fillMemoryNode();
        } catch (CannotQueryMemory e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        try {
            fillNetworkNode();
        } catch (CannotQueryNetwork e) {
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        try {
            fillExtraNode();
        } catch (CannotQueryExtra e){
            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        hideNode(6);
        hideNode(7);
        hideNode(8);
        hideAdvertiseWindow();
    }

    private void hideAdvertiseWindow() {
        adView.setVisibility(View.GONE);
    }

    private void fillDeviceNode() throws CannotQueryDevice {
        TextView t_device = (TextView) findViewById(R.id.device_device);
        TextView t_deviceBrand = (TextView) findViewById(R.id.device_brand);
        TextView t_deviceModel = (TextView) findViewById(R.id.device_model);
        TextView t_deviceSerialNumber = (TextView) findViewById(R.id.device_serial_number);
        TextView t_deviceDisplay = (TextView) findViewById(R.id.device_display);
        TextView t_deviceIMEI = (TextView) findViewById(R.id.deviceIMEI);

        NetworkImplementation networkImplementation = new NetworkImplementation(getApplicationContext());

        Device device = new Device();
        t_device.setText("Device codename: " + device.codeName());
        t_deviceBrand.setText("Brand: " + device.brandName());
        t_deviceModel.setText("Model: " + device.modelName());
        t_deviceSerialNumber.setText("Serial no.: " + device.serialNumber());
        t_deviceDisplay.setText("Build number: " + device.deviceBuildId());
        t_deviceIMEI.setText("IMEI: " + networkImplementation.getDeviceId());
    }

    private void fillSystemNode() throws CannotQuerySystem {
        TextView t_systemAndroid = (TextView) findViewById(R.id.system_system);
        TextView t_systemSDK = (TextView) findViewById(R.id.system_sdk);

        DeviceOS system = new DeviceOS();
        t_systemAndroid.setText("OS: " + system.systemVersion());
        t_systemSDK.setText("SDK: " + system.systemSDKVersion());
    }

    private void fillNetworkNode() throws CannotQueryNetwork {
        TextView networkInfo = (TextView) findViewById(R.id.m_node3_line1);

        NetworkImplementation network = new NetworkImplementation(getApplicationContext());

        networkInfo.setText(
                " Operator name: " + network.getNetworkOperatorName() + "\n" +
                " Sim operator name: " + network.getSimOperatorName() + "\n" +
                " Subscriber ID (IMSI): " + network.getSubscriberId() + "\n" +
//                " Phone type: " + network.getPhoneType() + "\n" +
                " Mobile data: " + decideOnDataState(network.getDataState())
        );
    }

    private String decideOnDataState(int dataState) {
        String state = "";
        if(dataState == 0){
            state = "Disconnected";
        } else if(dataState == 2){
            state = "Connected";
        } else if(dataState == 3){
            state = "Suspended/No internet";
        } else if(dataState == 1){
            state = "Connecting";
        }
        return state;
    }

    private void fillWiFiNode() throws CannotQueryWifi {
        TextView t_wifiState = (TextView) findViewById(R.id.wifi_state);
        TextView t_wifiMac = (TextView) findViewById(R.id.wifi_mac);
        if (SDK_INT >= 23) {
            //NODE is hidden if you run on SDK 23+, we'll fix that in 1.1
            buttonList[3].setVisibility(View.GONE);
            layoutList[3].setVisibility(View.GONE);
            //Toast.makeText(getApplicationContext(),"API 23 not supported yet, check change list for info.", Toast.LENGTH_SHORT).show();
            t_wifiState.setVisibility(View.GONE);
            t_wifiMac.setVisibility(View.GONE);
        } else {
            WiFiImplementation wifi = new WiFiImplementation(getApplicationContext());
            if (wifi.wifiExist() && wifi.getWiFiState() != null) {
                if(wifi.getWiFiState() == 1){
                    t_wifiState.setText("WiFi Disabled");
                } else {
                    t_wifiState.setText(
                            "WiFi state: " + decideOnWiFiState(wifi.getWiFiState()) + "\n" +
                            "Current SSID: " + wifi.getCurrentConnectedSSID()
                    );
                }
                t_wifiMac.setText("WiFi MAC:" + wifi.getMacAddress());
            }
        }
    }

    private String decideOnWiFiState(Integer wiFiState) {
        String state = "";
        if(wiFiState == 3){
            state = "Enabled";
        } else if(wiFiState == 0){
            state = "Turning Off";
        } else if(wiFiState == 2){
            state = "Turning On";
        } else if(wiFiState == 4){
            state = "Unknown";
        }
        return state;
    }

    private void fillBluetoothNode() throws CannotCheckBluetooth {
        TextView t_bluetooth_mac = (TextView) findViewById(R.id.bluetooth_mac);
        if (SDK_INT >= 23) {
            //NODE is hidden if you run on SDK 23+, we'll fix that in 1.1
            buttonList[4].setVisibility(View.GONE);
            layoutList[4].setVisibility(View.GONE);
            //Toast.makeText(getApplicationContext(),"API 23 not supported yet, check change list for info.", Toast.LENGTH_SHORT).show();
            t_bluetooth_mac.setVisibility(View.GONE);
        } else {
            BluetoothImplementation bluetooth = new BluetoothImplementation();
            t_bluetooth_mac.setText("MAC: " + bluetooth.getMacAddress());
        }
    }

    private String decideOnBatteryHealth(int value) {
        if (value != 1) {
            String returnedValue = "";
            if (value == 7) {
                returnedValue = "Battery is too cold";
            }
            if (value == 4) {
                returnedValue = "Battery should be replaced";
            }
            if (value == 2) {
                returnedValue = "Good condition";
            }
            if (value == 3) {
                returnedValue = "Battery is overheating";
            }
            return returnedValue;
        } else {
            return " unknown ";
        }
    }

    private String decideOnBatteryStatus(int value) {
        if (value != 1) {
            String returnedValue = "";
            if (value == 2) {
                returnedValue = "Charging";
            }
            if (value == 3) {
                returnedValue = "Discharging";
            }
            if (value == 5) {
                returnedValue = "FULL";
            }
            if (value == 4) {
                returnedValue = "Not Charging";
            }
            return returnedValue;
        } else {
            return " unknown ";
        }
    }

    private BroadcastReceiver batteryInfoReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int batteryHealth = intent.getIntExtra(BatteryManager.EXTRA_HEALTH, 0);
            //int icon_id = intent.getIntExtra(BatteryManager.EXTRA_ICON_SMALL,0);
            int batteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            int batteryPlugged = intent.getIntExtra(BatteryManager.EXTRA_PLUGGED, 0);
            boolean batteryPresent = intent.getExtras().getBoolean(BatteryManager.EXTRA_PRESENT);
//          int batteryScale = intent.getIntExtra(BatteryManager.EXTRA_SCALE,0);
            int batteryStatus = intent.getIntExtra(BatteryManager.EXTRA_STATUS, 0);
            String batteryTechnology = intent.getExtras().getString(BatteryManager.EXTRA_TECHNOLOGY);
            int batteryTemperature = intent.getIntExtra(BatteryManager.EXTRA_TEMPERATURE, 0);
            int batteryVoltage = intent.getIntExtra(BatteryManager.EXTRA_VOLTAGE, 0);

            Double voltage = Double.parseDouble(String.valueOf(batteryVoltage));
            Double temperature = Double.parseDouble(String.valueOf(batteryTemperature));

            String batteryActualHealth = decideOnBatteryHealth(batteryHealth);
            String batteryActualStatus = decideOnBatteryStatus(batteryStatus);

            batteryInfo.setText(
                    " Health: " + batteryActualHealth + "\n" +
                            " Level: " + batteryLevel + "%\n" +
                            " Charging: " + (batteryPlugged == 0 ? "No" : "Yes") + "\n" +
                            " Present: " + (batteryPresent == false ? " External source " : "Yes") + "\n" +
                            " Status: " + batteryActualStatus + "\n" +
                            " Voltage: " + (voltage / 1000) + "V\n" +
                            " Temperature: " + (temperature / 10) + "\u00b0" + "C" + "\n" +
                            " Technology: " + batteryTechnology + "\n"
            );
        }
    };

    private void fillAccountsNode() throws CannotQueryAccounts {
        hideNode(6);
        //TODO Implement in next version
    }

    private void fillMemoryNode() throws CannotQueryMemory {
//        TextView memoryInfo = (TextView) findViewById(R.id.m_node8_line1);
//        Memory memory = new Memory();
//        memoryInfo.setText(
//                " Total Memory: " + memory.getTotalMemory() / 1048576L + "\n" +
//                        " Available: " + memory.getAvilableMemory() / 1048576L + "%\n"
//        );
        hideNode(7);
        //TODO Implement in next version

    }

    private void fillDevNode() throws CannotQueryDev {
        hideNode(8);
        //TODO Implement in next version
    }

    private void fillExtraNode() throws CannotQueryExtra {
        TextView extra = (TextView) findViewById(R.id.m_node10_line1);
        Extras extras = new Extras(getApplicationContext());
        extra.setText(
                "Unique device ID: " + extras.getDeviceID()
        );
    }

    private void fillSensorsNode() throws CannotCheckSensor {
        //this will be implemented in later versions of app
    }
}
