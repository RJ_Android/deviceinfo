package main.nl.gingercat.deviceinfo.ClassPackage;

import android.content.Context;
import android.provider.Settings.Secure;

/**
 * Created by jacek on 11/18/15.
 */
public class Extras implements main.nl.gingercat.deviceinfo.Interfaces.Extras {

    Context context;

    public Extras(Context context) {
        this.context = context;
    }

    @Override
    public String getDeviceID() {
        return Secure.getString(context.getContentResolver(), Secure.ANDROID_ID);
    }
}
