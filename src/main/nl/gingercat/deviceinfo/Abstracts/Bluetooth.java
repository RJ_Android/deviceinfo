package main.nl.gingercat.deviceinfo.Abstracts;

import main.nl.gingercat.deviceinfo.Interfaces.Connectivity;

/**
 * Created by jacek on 11/18/15.
 */
public abstract class Bluetooth implements Connectivity{
    @Override
    public abstract String getMacAddress();
}
