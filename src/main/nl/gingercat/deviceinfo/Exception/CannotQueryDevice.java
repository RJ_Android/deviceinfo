package main.nl.gingercat.deviceinfo.Exception;

/**
 * Created by Jacky on 2015-11-15.
 */
public class CannotQueryDevice extends Exception {
    public CannotQueryDevice() {
        super();
    }

    public CannotQueryDevice(String detailMessage) {
        super(detailMessage);
    }

    public CannotQueryDevice(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CannotQueryDevice(Throwable throwable) {
        super(throwable);
    }
}
