package main.nl.gingercat.deviceinfo.Interfaces;

/**
 * Created by jacek on 11/18/15.
 */
public interface DeviceOS {
    String systemVersion();
    int systemSDKVersion();

}
