package main.nl.gingercat.deviceinfo.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import nl.gingercat.DeviceInfo.R;


/**
 * Created by Jacky on 2015-08-31.
 */
public class Splash extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
//        MediaPlayer player = MediaPlayer.create(this, R.raw.splash_moew);
//        player.start();
        Thread splash = new Thread(){
            public void run(){
                try{
                    short timer = 1000;
                    sleep(timer);
                    startActivity(new Intent("deviceInfo.mainActivity"));
                }
                catch(InterruptedException e){
                    e.printStackTrace();
                }
                finally {
                    finish();
                }
            }
        };
        splash.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}