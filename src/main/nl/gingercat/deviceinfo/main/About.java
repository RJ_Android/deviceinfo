package main.nl.gingercat.deviceinfo.main;

import android.app.Activity;
import android.os.Bundle;
import nl.gingercat.DeviceInfo.R;


/**
 * Created by Jacky on 2015-09-01.
 */
public class About extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about);
    }
}