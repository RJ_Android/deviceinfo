# Welcome

This is a page about project DeviceInfo App. Here will be explained the main reason and features.

This repository is under git versio control, basic origin is: 
```
https://<username>@bitbucket.org/RJ_Android/deviceinfo.git
```
Wiki page is under git as well, origin:

```
https://<username>@bitbucket.org/RJ_Android/deviceinfo.git/wiki
```

## Basic App explanation

The main purpose of the app is " single screen --> all info " 
User have access to all data which may be relavant for him.

## Features explanation

App is builded with nodes which contains particular data for example: 
Node "System info" should contain only info about current OS installed in device.
And so on.

## Features to implement (node:feature)  " ADD if you have an idea " 


Device:
- brand of device (LG, Samsung, etc)
- model of device
- year of production
- time of purchase
- runtime of device

System: 
- version of android with SDK level
- compilation number
- kernel
- system up time

Phone: (network)
- network signal
- current carrier
- all info about antena and power we can get

WiFi:
- MAC
- current network
- proxy info
- networks saved in device

Bluetooth:
- MAC
- maybe some more info

Battery info:
- temparature
- condition
- health

Accounts:
- e-mails
- other account in device

Memory info:
- info about internal memory
- info about external memory
- RAM stats

Dev Tools:
- have device activated dev options

Extra:
- is device rooted
- is device unlocked/locked
- AndroidID - special Id generated with first run by android
- AdsId
- GCM ID