package main.nl.gingercat.deviceinfo.Interfaces;

/**
 * Created by jacek on 11/18/15.
 */
public interface Storage {

    String getMemoryInfo();
    Long getAvilableMemory();
    Long getUsedMemory();
    Long getFreeMemory();
    Long getTotalMemory();

}
