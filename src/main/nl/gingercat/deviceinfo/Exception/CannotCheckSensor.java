package main.nl.gingercat.deviceinfo.Exception;

/**
 * Created by jacek on 11/22/15.
 */
public class CannotCheckSensor extends Exception {
    public CannotCheckSensor() {
        super();
    }

    public CannotCheckSensor(String detailMessage) {
        super(detailMessage);
    }

    public CannotCheckSensor(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CannotCheckSensor(Throwable throwable) {
        super(throwable);
    }
}
