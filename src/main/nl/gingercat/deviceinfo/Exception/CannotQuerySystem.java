package main.nl.gingercat.deviceinfo.Exception;

/**
 * Created by Jacky on 2015-11-15.
 */
public class CannotQuerySystem extends Exception {
    public CannotQuerySystem() {
        super();
    }

    public CannotQuerySystem(String detailMessage) {
        super(detailMessage);
    }

    public CannotQuerySystem(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CannotQuerySystem(Throwable throwable) {
        super(throwable);
    }
}
