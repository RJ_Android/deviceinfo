package main.nl.gingercat.deviceinfo.ClassPackage;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.widget.Toast;
import main.nl.gingercat.deviceinfo.Abstracts.WiFi;
import main.nl.gingercat.deviceinfo.Exception.CannotQueryWifi;

/**
 * Created by jacek on 11/18/15.
 */
public class WiFiImplementation extends WiFi {

    WifiManager wifiManager;
    Context context;

    public WiFiImplementation(Context context) {
        wifiManager = (WifiManager) context.getSystemService(context.WIFI_SERVICE);
        this.context = context;
    }

    @Override
    public String getMacAddress() {
        if(wifiManager == null) {
            try {
                throw new CannotQueryWifi("WiFi not supported by device");
            } catch (CannotQueryWifi cannotQueryWifi) {
                cannotQueryWifi.printStackTrace();
            }
        }
        if(wifiManager.isWifiEnabled()){
            return wifiManager.getConnectionInfo().getMacAddress();
        }
        else{
            //WE CAN READ MAC ADDRESS FROM FILE IN SYSTEM LEFT IT FOR NEXT VERSION
            Toast.makeText(context,"WiFi is OFF",Toast.LENGTH_SHORT).show();
            return "WiFi OFF";
        }
    }

    @Override
    public boolean wifiExist() {
        return true;
    }

    @Override
    public String getCurrentConnectedSSID(){
        return wifiManager.getConnectionInfo().getSSID();
    }

    public Integer getWiFiState() throws CannotQueryWifi {
        if(wifiManager == null){
            return wifiManager.getWifiState();
        }
            return 0;
    }
}
