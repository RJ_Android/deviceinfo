package main.nl.gingercat.deviceinfo.Exception;

/**
 * Created by Jacky on 2015-11-15.
 */
public class CannotQueryWifi extends Exception {
    public CannotQueryWifi() {
        super();
    }

    public CannotQueryWifi(String detailMessage) {
        super(detailMessage);
    }

    public CannotQueryWifi(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CannotQueryWifi(Throwable throwable) {
        super(throwable);
    }
}
