package main.nl.gingercat.deviceinfo.Exception;

/**
 * Created by Jacky on 2015-11-15.
 */
public class CannotQueryNetwork extends Exception {
    public CannotQueryNetwork() {
        super();
    }

    public CannotQueryNetwork(String detailMessage) {
        super(detailMessage);
    }

    public CannotQueryNetwork(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CannotQueryNetwork(Throwable throwable) {
        super(throwable);
    }
}
