package main.nl.gingercat.deviceinfo.Exception;

/**
 * Created by Jacky on 2015-11-15.
 */
public class CannotQueryExtra extends Exception {
    public CannotQueryExtra(Throwable throwable) {
        super(throwable);
    }

    public CannotQueryExtra(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CannotQueryExtra(String detailMessage) {
        super(detailMessage);
    }

    public CannotQueryExtra() {
        super();
    }
}
