package main.nl.gingercat.deviceinfo.Exception;

/**
 * Created by Jacky on 2015-11-15.
 */
public class CannotCheckBluetooth extends Exception {
    public CannotCheckBluetooth() {
        super();
    }

    public CannotCheckBluetooth(String detailMessage) {
        super(detailMessage);
    }

    public CannotCheckBluetooth(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public CannotCheckBluetooth(Throwable throwable) {
        super(throwable);
    }
}
